There are two pages for two tasks .

index.html : for task 1 (Making a simple website with vanilla JS)

slider.html : for task 2 (Lazy Loading To Avoid Pagination (vanilla js))

Both tasks are deployed in same link.

Deployment Link: https://nextgrowth-frontend-project-rp7007-a1413322e0115688983f79ec3911.gitlab.io/

Click on lazy-loading page to see second task
